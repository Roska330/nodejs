const express = require('express');
// para establecer las distintas rutas, necesitamos instanciar el express router
var router = express.Router();

const cervezasController = require("../controllers/cervezasController.js")

router.get('/',(req, res) => {
  console.log('/cervezas')
  cervezasController.index(req,res);
})

router.get('/:id',(req, res) => {
  console.log('/cervezas/id')
  cervezasController.show(req,res);
})

router.post('/',(req, res) => {
  cervezasController.store(req,res);
})

router.delete('/:id',(req, res) => {
  cervezasController.destroy(req,res);
})
router.put('/:id',(req, res) => {
  cervezasController.update(req,res);
})

  module.exports = router;