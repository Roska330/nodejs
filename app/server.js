var express = require('express'); //llamamos a Express
var app = express();

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const router = require("./routes.js");

var port = process.env.PORT || 8080;  // establecemos nuestro puerto

app.use('/api', router);


// iniciamos nuestro servidor
app.listen(port, () =>{
    console.log('API escuchando en el puerto ' + port)
});


        
