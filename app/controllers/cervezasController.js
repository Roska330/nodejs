const mysql = require("mysql2");
const connection = mysql.createConnection( {
    user: 'root',
    password: 'root',
    host: 'localhost',
    database: 'cervezas'
})

const index = function(req, res) {
    console.log("Lista de cervezas")
    const sql = 'SELECT * FROM cervezas';
    connection.query(sql,function (err,result,fields) {
        if(err)      
            res.status(500).json(err)         
        res.json(result)
        
        
    })
    // res.json({ mensaje: "¡Lista de cervezas desde cervezasController" })
}

const show = function(req, res) {
    const id = req.params.id
    const sql = 'SELECT * FROM cervezas WHERE id = ?';
    connection.query(sql,[id],(err,result,fields) => {
        if(err)     
            res.status(500).json(err)        
        res.json(result)
        
    })
    // res.json({ mensaje: "¡Detalle de cervezas desde cervezasController" })
}

const store = function(req, res) {
    
    const alcohol = req.body.alcohol;
    const container = req.body.container;
    const description = req.body.description;
    const name = req.body.name;
    const price = req.body.price;

    console.log(alcohol + " " + container + " " + description + " " + name + " " + price )


    const sql = 'INSERT INTO cervezas (alcohol,container,description,name,price) ' + 
    'VALUES(?,?,?,?,?)';
    connection.query(sql,[alcohol,container,description,name,price],(err,result,fields) => {
        if(err)     
            res.status(500).json(err)        
        res.json(result)
        
    })
    //res.json ({ mensaje: "¡Creacion de cervezas desde cervezasController" })
}

const destroy = function(req, res) {
    const id = req.params.id;
    const sql = 'DELETE FROM cervezas WHERE id = ?'
    connection.query(sql,[id],(err,result,fields) => {
        if(err)     
            res.status(500).json(err)        
        res.json(result)
        
    })



    //res.json ({ mensaje: "¡Eliminacion de cervezas desde cervezasController" })
}

const update = function(req, res) {
    
    const id = req.params.id;
    const alcohol = req.body.alcohol;
    const container = req.body.container;
    const description = req.body.description;
    const name = req.body.name;
    const price = req.body.price;

    console.log(alcohol + " " + container + " " + description + " " + name + " " + price )


    const sql = 'UPDATE cervezas SET alcohol = ?, container = ?, description = ?, name = ?, price = ? WHERE id = ?';
    connection.query(sql,[alcohol,container,description,name,price,id],(err,result,fields) => {
        if(err)     
            res.status(500).json(err)        
        res.json(result)
        
    })


    //res.json({ mensaje: "¡Actualizacion de cervezas desde cervezasController" })
}

module.exports = {
    index,
    show,
    store,
    destroy,
    update
}